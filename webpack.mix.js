/*
    Basic webpack setup for your project.
    Mainly gets the root folder of your project, 
    for the browser sync to work correctly.
    Minifies js, css and processes sass as well as
    copies files from src/ folder to dist/ folder.   
 */

var path = require('path'),
    appDir = path.dirname(require.main.filename),
    index_length = path.dirname(appDir).split(path.sep).length,
    appRoot = path.dirname(appDir).split(path.sep)[index_length - 3]; 

const {mix} = require('laravel-mix');

mix.setPublicPath('dist');
mix.copy('src/assets/img/*', 'dist/img');
mix.copy('src/*.php', 'dist')
   .copy('src/assets/js/vendors/slider.js', 'dist/js/vendors');

mix
   .js('src/assets/js/main.js', 'dist/js')
   .sass('src/assets/sass/main.scss', 'dist/css')
   .options({
      processCssUrls: false     // Fix for the css relative urls to work correctly
   })
   .sourceMaps()   
   .browserSync({
     injectChanges: true,
     files: [ 'src/**/*.js', 'src/**/*.scss','src/**/*.php'],
     logSnippet: true,
     proxy: {target: 'http://' + appRoot + '.test/dist/'},
   });