# Lightweight Slider plugin

Simple slider plugin with several handy options. 
Main file `dist/js/vendors/slider.js`

## Boilerplate with standalone laravel-mix and bootstrap

This is just a very simple boilerplate to quick start any mini web project using 
laravel mix, laravel valet, bootstrap and sass, js preprocessors with browsersync.


## Installation

You will have to install laravel valet first, for the server and routing to properly work.

- [Homebrew](http://sourabhbajaj.com/mac-setup/Homebrew/) (so you can install other dependencies)
- composer (for proper installation of valet)

```
$ brew install composer
```

- [npm](http://sourabhbajaj.com/mac-setup/Node.js/)
- php7+

```
brew install homebrew/php/php72
```

- [Laravel valet](https://laravel.com/docs/5.5/valet#installation)
    - install
    - start valet
    - park your valet folder (project folder) f.e. Sites/ if your project is inside this folder

### To install all dependencies:

```
npm install
```

## Usage

Run all tasks:

```
npm run dev
```

Run all tasks and minify:

```
npm run production
```

Run all tasks and watch for changes (first run this will install additional dependencies):

```
npm run watch
```