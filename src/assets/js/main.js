/**
 * Main js file
 */

var testimonial_slider = new testimonial.slider({
    selector: ['.testimonial-slider'],
    slideshow: 5000,
    effect: 'fade'
});