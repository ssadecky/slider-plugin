/*!
* slider.js
* @author  svetozar_sadecky
* @version 0.0.1
* @url
*/
(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.testimonial = factory();
    }
}(this, function() {

    /* ----------------------------------------------------------- */
    /* == Slider */
    /* ----------------------------------------------------------- */
    
    var transitionEvent = whichTransitionEvent();

    // Check what transform vendor is needed
    var transformProp = (function(){
      var testEl = document.createElement('div');
      if(testEl.style.transform == null) {
        var vendors = ['Webkit', 'Moz', 'ms'];
        for(var vendor in vendors) {
          if(testEl.style[ vendors[vendor] + 'Transform' ] !== undefined) {
            return vendors[vendor] + 'Transform';
          }
        }
      }
      return 'transform';
    })(); 

    // Check what transition vendor is needed
    var transitionProp = (function(){
      var testEl = document.createElement('div');
      if(testEl.style.transition == null) {
        var vendors = ['Webkit', 'Moz', 'ms'];
        for(var vendor in vendors) {
          if(testEl.style[ vendors[vendor] + 'Transition' ] !== undefined) {
            return vendors[vendor] + 'Transition';
          }
        }
      }
      return 'transition';
    })(); 
    
    /**
     * Slider function
     * @param {array} options Array of options
     * Available Options:
     * slideshow[number/boolean]: [number of miliseconds] or false;
     * bubblenav[boolean];
     * animationTime[number]: [number of miliseconds];
     * selector[string]: use either classes, ids or element tags;
     * effect[string]: [slide, fade];
     */
    function Slider(options) {

        var defaults = {
            slideshow: false,
            bubblenav: true,
            animationTime: 500,
            selector: ['.slider'],
            effect: 'slide'
        };

        // extends config
        this.opts = extend({}, defaults, options);

        // init modal
        this.init();

    }
    /**
     * Slider initializer
     * @return {void}
     */
    Slider.prototype.init = function() {
        if (this.slider) {
            return;
        }
        // context fix
        var that = this;

        this.animationOver = true;

        // get slider element
        if (document.querySelector(this.opts.selector[0])) {
            this.element = document.querySelector(this.opts.selector[0]);   
        } else {
            return false;
        }
        this.quotes = [];

        [].forEach.call(
                this.element.children[0].children, function(child){
                    child.classList.contains('quote') && 
                        that.quotes.push(child); 
        });

        this.element.children[0].children[0].setAttribute('data-target', 'active');
        
        _bindEvents.call(this);

        // If this.opts.slideshow has value - init slideshow
        this.opts.slideshow && window.addEventListener('load', function() {
            _slideshowInit.call(that);
        }, false);

        // If bubble nav is turned on
        this.opts.bubblenav && this.initBubbleNav();

    };
    /**
     * Main navigation method that determines direction, and resets classes in some cases
     * @param  {Mouse_target} target Clicked navigation arrow
     * @return {void}        
     */
    Slider.prototype.nav = function(target) {
        var element = target.currentTarget;
        
        if (!this.animationOver) {
            return false;    
        }

        if (this.isSlideEffect()) {
          this.resetClasses();  
        }

        //If slideshow has interval setting - clear it here
        this.opts.slideshow && _clearInterval.call(this); 

        element.classList.contains('slide-left') ?
            this.slideLeft() : this.slideRight();    
    }
    /**
     * Core animation function with built in conditions for effects.
     * @param  {number} active_index This is active index taken from bubble navigation
     * @return {HTML_element} active This is current active element after navigation
     */
    Slider.prototype.slideLeft = function(active_index) {
        var that = this,
            prev_active = '',
            active,
            arr_length = this.quotes.length,
            timeout;

        this.animationOver = false;
        // Get previously active element
        [].forEach.call(this.quotes, function(child){
            if (child.getAttribute('data-target') == 'active') {
                prev_active = child;
            }
        });
        
        // Check if loopover
        if (active_index != null || active_index != undefined) {
            active = this.quotes[active_index];
        } else {
            if (!prev_active.previousElementSibling) {
                active = this.quotes[arr_length - 1]; 
            } else {
                active = this.quotes[ [].indexOf.call(this.quotes, prev_active ) - 1 ];
            }  
        }

        // Determine animation effect
        if (this.isSlideEffect()) 
        {
            active.classList.add('from-right');
        } 

        prev_active.style.display = 'block';
        active.setAttribute('data-target', 'active');

        if (this.isFadeEffect()) 
        {
            active.classList.add('faded');
        }

        // if bubble nav is activated
        this.bubbleNav && _moveBubble.call(this, active);

        setTimeout(function(){
            
            active.classList.add('in-motion');

            if (that.isSlideEffect()) 
            {
                prev_active.classList.add('from-left');
                transformAnimations(active, 'translateX(0px)', that.opts.animationTime);
                transformAnimations(prev_active, 'translateX(-100%)', that.opts.animationTime);
            } 
            else if (that.isFadeEffect()) 
            {
                prev_active.style.opacity = '0';
                setTransitions(prev_active, 'opacity', that.opts.animationTime);
                

                setTimeout(function(){ 
                    active.style.opacity = '1';
                    setTransitions(active, 'opacity', that.opts.animationTime);  
                }, that.opts.animationTime / 2);
            }
            
        }, 50);

        prev_active.setAttribute('data-target', '');

        // Reset all classes at the end of the transition
        setTimeout(function(){
            _resetAll.call(that, active);
        }, this.opts.animationTime);

        return active; 
    }
    /**
     * Core animation function with built in conditions for effects.
     * Also used as a default direction for slideshow
     * @param  {number} active_index This is active index taken from bubble navigation
     * @return {HTML_element} active This is current active element after navigation
     */
    Slider.prototype.slideRight = function(active_index) {
        var that = this,
            prev_active = '',
            active,
            arr_length = this.quotes.length;

        this.animationOver = false;

        if (!documentHasFocus()) {
           _clearInterval.call(this); 
           return false; 
        } 
        // Get previously active element
        [].forEach.call(this.quotes, function(child){
            if (child.getAttribute('data-target') == 'active') {
                prev_active = child;
            }
        });
        
        // Check if loopover
        if (active_index != null || active_index != undefined) {
            active = this.quotes[active_index];
        } else {
           if ( prev_active.nextElementSibling.classList.contains('bubble-nav') ) {
               active = this.quotes[0]; 
           } else {
               active = this.quotes[ [].indexOf.call(this.quotes, prev_active ) + 1 ];
           }  
        } 
        
        // Show elements that should be animated  
        if (this.isSlideEffect()) 
        {
            active.classList.add('from-left');
        } 

        prev_active.style.display = 'block';
        active.setAttribute('data-target', 'active');

        if (this.isFadeEffect()) 
        {
            active.classList.add('faded');
        }

        // if bubble nav is activated
        this.bubbleNav && _moveBubble.call(this, active);

        // Add transition classes to trigger correct animation
        setTimeout(function(){
            active.classList.add('in-motion');

            if (that.isSlideEffect()) {
                prev_active.classList.add('from-right');
                transformAnimations(active, 'translateX(0px)', that.opts.animationTime);
                transformAnimations(prev_active, 'translateX(100%)', that.opts.animationTime);
            } 
            else if (that.isFadeEffect()) 
            {    
                prev_active.style.opacity = '0';
                setTransitions(prev_active, 'opacity', that.opts.animationTime);

                setTimeout(function(){
                    active.style.opacity = '1';
                    setTransitions(active, 'opacity', that.opts.animationTime);
                }, that.opts.animationTime / 2); 
            }
        }, 50);

        // Remove active data label from previous element
        prev_active.setAttribute('data-target', '');

        // Reset all classes at the end of the transition
        setTimeout(function(){
            _resetAll.call(that, active);
        }, this.opts.animationTime);

        return active;
    }
    /**
     * Resets direction classes for all slider blocks
     * @return {void}
     */
    Slider.prototype.resetClasses = function() {
        for (var index in this.quotes) {
            this.quotes[index].classList.remove('from-left', 'from-right');
        }
    }
    /**
     * Initializes bubble navigation
     * by appending correct count of bubbles with its wrapper to the DOM
     * @return {void} 
     */
    Slider.prototype.initBubbleNav = function() {

        var div = document.createElement('div');

        div.setAttribute('class','bubble-nav');
        
        for (var i = 0; i < this.quotes.length; i++) {
            var bubble = document.createElement('div');
            bubble.setAttribute('class','bubble');
            bubble.addEventListener('click', this.slideTo.bind(this) );
            if (this.quotes[i].getAttribute('data-target') === 'active') {
                bubble.classList.add('active');
            }
            div.appendChild(bubble);
        }

        this.element.children[0].appendChild( div );

        this.bubbleNav = document.querySelectorAll(this.opts.selector + ' .bubble-nav .bubble');
    }
    /**
     * Method used by bubble navigation determines correct elements
     * to navigate from and to
     * @param  {mouse-event} e User click event
     * @return {void} 
     */
    Slider.prototype.slideTo = function(e) {

        var index = getElementIndex(e.target),
            prev_index = getElementIndex(this.getActiveElement());

        // if animation is not over, do not allow action
        if (!this.animationOver) {
            return false;    
        }

        this.resetClasses();

        //If slideshow has interval setting - clear it here
        this.opts.slideshow && _clearInterval.call(this); 

        if (index != prev_index) {
            if (  index > prev_index ) {
                this.slideRight(index);
            } else {
                this.slideLeft(index);
            } 
        }

    }
    /**
     * Gets current active element based on data-target attribute
     * @return {HTML_element} Active element
     */
    Slider.prototype.getActiveElement = function() {
        var active;
        [].forEach.call(this.quotes, function(child) {
            if (child.getAttribute('data-target') == 'active') {
                active = child;      
            }
        }); 

        return active;  
    }
    /**
     * Checks if effect option is 'slide'
     * @return {Boolean} 
     */
    Slider.prototype.isSlideEffect = function() {
        if (this.opts.effect === 'slide') {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Checks if effect option is 'fade'
     * @return {Boolean}
     */
    Slider.prototype.isFadeEffect = function() {
        if (this.opts.effect === 'fade')  {
            return true;
        } else {
            return false;
        }
    }

    /* ----------------------------------------------------------- */
    /* == private methods */
    /* ----------------------------------------------------------- */

    function extend() {
        for (var i = 1; i < arguments.length; i++) {
            for (var key in arguments[i]) {
                if (arguments[i].hasOwnProperty(key)) {
                    arguments[0][key] = arguments[i][key];
                }
            }
        }
        return arguments[0];
    }

    function _bindEvents() {
        var that = this;
        this._events = {
            navigate: this.nav.bind(this)
        };

        [].forEach.call(this.element.children, function(child) {
            child.classList.contains('arrow-nav') && 
                [].forEach.call(child.children, function(child) {
                    child.addEventListener('click', that._events.navigate);
                });          
        });

        [].forEach.call(this.quotes, function(child){
            child.addEventListener(whichTransitionEvent(), function(){
                _resetAll.call(that, this);
            });
        });

        window.addEventListener('scroll', function() {
            _clearInterval.call(that);
        }, false); 
        window.addEventListener('resize', function() {
            _clearInterval.call(that);
        }, false); 
        
    }

    function _slideshowInit() {
        var that = this;
        if(!this.interval) {
            this.interval = setInterval(function() { 
                that.resetClasses();
                that.slideRight(); 
            }, this.opts.slideshow);
        }  
    }

    function _clearInterval() {
        clearInterval(this.interval);
        this.interval = false;
        _slideshowInit.call(this);
    }

    function _moveBubble(active) {
        [].forEach.call(this.bubbleNav, function(child){
            child.classList.remove('active');    
        });
        this.bubbleNav[getElementIndex(active)].classList.add('active');
    }

    function whichTransitionEvent() {
        var t;
        var el = document.createElement('slider-test-transition');
        var transitions = {
            'transition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'MozTransition': 'transitionend',
            'WebkitTransition': 'webkitTransitionEnd'
        };

        for (t in transitions) {
            if (el.style[t] !== undefined) {
                return transitions[t];
            } else {
                return false;
            }
        }
    }

    function transformAnimations(element, value, time) { 
        element.style[transformProp] = value;
        setTransitions(element, 'transform', time);
    }

    function setTransitions(element, property, time) {
        var toSeconds = time / 1000;
        element.style[transitionProp] = property + ' ' + toSeconds + 's';
    }

    function _resetAll(element) {
        var that = this;

        for (var index in this.quotes) {
            setAttributes(this.quotes[index], {
                'style': ''
            });  
        }
        
        element.classList.remove('in-motion', 'from-left', 'from-right', 'faded');
        setTimeout(function(){
          that.animationOver = true;  
        }, 250);  
    }

    function setAttributes(el, attrs) {
      for(var key in attrs) {
        el.setAttribute(key, attrs[key]);
      }
    }

    function getElementIndex(node) {
        var index = 0;
        while ( (node = node.previousElementSibling) ) {
            index++;
        }
        return index;
    }

    function documentHasFocus() {
        return document.hasFocus();
    }
    /* ----------------------------------------------------------- */
    /* == return */
    /* ----------------------------------------------------------- */

    return {
        slider: Slider
    };

}));